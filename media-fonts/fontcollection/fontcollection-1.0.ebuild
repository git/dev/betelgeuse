# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# Header: $

DESCRIPTION="Betelgeuse's font collection"
HOMEPAGE="http://a.bo.cx/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"
IUSE=""

RDEPEND="
	media-fonts/artwiz-aleczapka-en
	media-fonts/corefonts
	media-fonts/freefonts
	media-fonts/sharefonts
	media-fonts/terminus-font
	media-fonts/ttf-bitstream-vera
	media-fonts/urw-fonts
	media-fonts/unifont"

