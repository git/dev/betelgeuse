inherit eutils

DESCRIPTION="Partial/differential file download client over HTTP which uses the rsync algorithm"
HOMEPAGE="http://zsync.moria.org.uk/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="x86"

src_install() {
	dobin zsync zsyncmake
	dodoc COPYING NEWS README
	doman doc/zsync.1 doc/zsyncmake.1
}

