# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
ESVN_REPO_URI="http://llvm.org/svn/llvm-project/libcxx/trunk"

inherit subversion

DESCRIPTION="libc++ is a new implementation of the C++ standard library, targeting C++0X"
HOMEPAGE="http://libcxx.llvm.org/"
SRC_URI=""

LICENSE="|| ( UoI-NCSA MIT )"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="sys-devel/clang"
RDEPEND=""

src_compile() {
	cd lib || die
	./buildit || die "compiling failed"
}

src_install() {
	dolib lib/libc++.so.1.0
	dosym libc++.so.1.0 /usr/lib/libc++.so.1
	dosym libc++.so.1 /usr/lib/libc++.so
	insinto /usr/include/c++/v1
	doins -r include/*
}

# they seem to rely on it being already installed
RESTRICT="test"

src_test() {
	cd test || die
	./testit || die "tests failed"
}
