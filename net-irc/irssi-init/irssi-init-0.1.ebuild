# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Init script for irssi"
HOMEPAGE="http://a.bo.cx/ebuilds/net-irc/irssi-init"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

IUSE=""

DEPEND=""

src_install() {
	newconfd ${FILESDIR}/${PV}/conf.d irssi
	newinitd ${FILESDIR}/${PV}/init.d irssi
}
