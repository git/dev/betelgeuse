#!/sbin/runscript

depend() {
	use net
}

start() {
	for user in ${IRSSI_USERS}; do 
		ebegin "Starting irssi session for ${user}"
		/bin/su - ${user} -c "screen -dmS irssi -- irssi"
		eend $? "Failed to start irssi for ${user}"
	done
}

