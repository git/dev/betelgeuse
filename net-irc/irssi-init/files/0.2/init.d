#!/sbin/runscript

depend() {
	use net
}

start() {
	local uni
	local value="${unicode:-${UNICODE}}"
	[ "${value}" = YES -o "${value}" = yes ] && uni=U
	for user in ${IRSSI_USERS}; do 
		ebegin "Starting irssi session for ${user}"
		/bin/su - ${user} -c "screen -${uni}dmS irssi -- irssi"
		eend $? "Failed to start irssi for ${user}"
	done
}

